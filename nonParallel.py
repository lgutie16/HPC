import sys
import glob
import os
import time


def hamming2(str1, str2):
    diffs = 0
    for ch1, ch2 in zip(str1, str2):
        if ch1 != ch2:
            diffs += 1
    return diffs
#from http://code.activestate.com/recipes/499304-hamming-distance/


def createFile(content):
    file = open('results-ham-gut.txt','w')
    for i in range(len(content)):
        file.write(content[i]['file']+'\n')
        distances = content[i]['distances']
        top = 0
        if(len(distances) <= 20):
            top = len(distances)
        else:
            top = 20
        for j in range(top):
            fileToCompare = distances[j]['file']
            distance = distances[j]['distance']
            file.write('    '+fileToCompare+' : '+str(distance)+'\n')
    file.write("--- %s seconds ---" % (time.time() - start_time))
    file.close()


#calc distance between all docs
def calcDistances():
    results = []
    i = 0
    j = 0
    for i in range(len(docs)):
        resultsAux = []
        for j in range (len(docs)):
            if(docs[i] != docs[j]):
                distance = hamming2(docs[i]['content'], docs[j]['content'])
                distanceDoc = {'file':docs[j]['filename'], 'distance': distance}
                resultsAux.append(distanceDoc)
        resultsAux.sort()
        resultsDoc = {'file':docs[i]['filename'], 'distances':resultsAux}
        results.append(resultsDoc)
    #print results
    createFile(results)


#read docs from a dir
start_time = time.time()
if len(sys.argv) > 1:
    path = sys.argv[1]
    docs = []
    i = 1
    for filename in glob.glob(os.path.join(path, '*.txt')):
        print 'reading doc '+str(i)
        docAux = open(filename, 'r').read().replace('\n', ' ')
        path, name = os.path.split(filename)
        info = {'filename':name, 'content':docAux}
        docs.append(info)
        i = i + 1  
    calcDistances()
else:
    print "First you have to say a directory"

