Here you are going to find two versions of the same algorithm (serial and parallel), these are for comparing similarity between a certain number of documents. 

**Before:**
Make sure you’ve installed Python 2.7 and mpi4py

**To run:** 
	**Serial:**

	$ python2.7 nonParallel.py <docs_dir>

Where *docs_dir* is the directory which contains the documents that will be compared
	Once it’s done it will create a document call “results-ham-gut.txt”
	
**Parallel:**
	
	$ mpiexec -np <nproc> python2.7 parallel.py <docs_dir>
Where *nproc* are the number of processors in which the program will be executed *docs_dir* is the directory which contains the documents that will be compared
Once it’s done it will create a document call “res-ham-gut-parallel-n<nproc>.txt”

**Slurm:**

If you have an slurm environment set up run the following command

    $ sbatch run.sbatch
Make sure you edit the run.sbatch for your own configuration

**Autores**

* Santiago Rodriguez Parra
* Linda Jessica Gutierrez
* Maria Alejandra Colorado