import sys
import glob
import os
import time

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

def hamming2(str1, str2):
    diffs = 0
    for ch1, ch2 in zip(str1, str2):
        if ch1 != ch2:
            diffs += 1
    return diffs
#from http://code.activestate.com/recipes/499304-hamming-distance/

def createFile(content):
    file = open('res-ham-gut-parallel-n'+str(size)+'.txt','w')
    file.write(content)
    file.write("--- %s seconds ---" % (time.time() - start_time))
    file.close()

def createFileContent(content):
    result = ""
    for i in range(len(content)):
        result = result + content[i]['file']+'\n'
        distances = content[i]['distances']
        top = 0
        if(len(distances) <= 20):
            top = len(distances)
        else:
            top = 20
        for j in range(top):
            fileToCompare = distances[j]['file']
            distance = distances[j]['distance']
            result = result + '    '+fileToCompare+' : '+str(distance)+'\n'
    return result

#calc distance between all docs
def calcDistances(docsProc, allDocs):
    results = []
    i = 0
    j = 0
    for i in range(len(docsProc)):
        resultsAux = []
        for j in range (len(allDocs)):
             if(docsProc[i] != allDocs[j]):
                distance = hamming2(docsProc[i]['content'], allDocs[j]['content'])
                distanceDoc = {'file':allDocs[j]['filename'], 'distance': distance}
                resultsAux.append(distanceDoc)
        resultsAux.sort()
        resultsDoc = {'file':docsProc[i]['filename'], 'distances':resultsAux}
        results.append(resultsDoc)

    return createFileContent(results)
    #print results


start_time = time.time()
#read docs from a dir
if len(sys.argv) > 1:
    path = sys.argv[1]
    docs = []
    for filename in glob.glob(os.path.join(path, '*.txt')):
        docAux = open(filename, 'r').read().replace('\n', ' ')
        path, name = os.path.split(filename)
        info = {'filename':name, 'content':docAux}
        docs.append(info)
    
    if rank == 0:
        nDocsProc = len(docs)/size
        for x in range(size-1):
            start = nDocsProc*x
            end = nDocsProc*(x+1)
            dest = x+1
            comm.send(docs[int(start):int(end)], dest=dest)
        results = calcDistances(docs[nDocsProc*(size-1):len(docs)], docs)
        for x in range(1,size):
            res = comm.recv(source = x)
            results = results + res
        createFile(results)
        print("--- %s seconds ---" % (time.time() - start_time))

    else:
        docsProc = comm.recv(source=0)
        result = calcDistances(docsProc, docs)
        comm.send(result, dest=0)

else:
    print("First you have to say a directory")